//---------------------------------------------------
//Functions
//---------------------------------------------------

function offsetHalf() {
    try {
        activeDoc = app.activeDocument;
        app.preferences.rulerUnits = Units.PIXELS;
        activeDoc.activeLayer.applyOffset(activeDoc.width/2,activeDoc.height/2,OffsetUndefinedAreas.WRAPAROUND);

        return 1;
    }catch(err) {
        return err;
    }
}

function equalizeImage(valueEqualize){
    //https://tolas.wordpress.com/2009/05/26/tutorial-how-to-equalize-textures-in-photoshop/
    try {

        activeDoc = app.activeDocument;
        app.preferences.rulerUnits = Units.PIXELS;
        activeDoc.activeLayer
        activeDoc.activeLayer.duplicate(activeDoc.activeLayer,ElementPlacement.PLACEAFTER).applyAverage();
        activeDoc.activeLayer.opacity=50;
        activeDoc.activeLayer.blendMode  = BlendMode.LINEARLIGHT;
        activeDoc.activeLayer.applyHighPass = valueEqualize;

        return 1;
    }catch(err) {
        return err;
    }
}

function createMaps(extensionRoot){
    try {
        var Script1 = File(extensionRoot + "/jsx/NormalToAO.jsx");
        $.evalFile (Script1);
        return 1;
    }catch(err) {
        return err;
    }
}
