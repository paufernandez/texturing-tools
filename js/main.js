(function () {
    'use strict';

    // Get a reference to a CSInterface object
    var csInterface = new CSInterface();
    console.log("csInterface loaded Correctly");

    //Get System Root
    var extensionRoot = csInterface.getSystemPath(SystemPath.EXTENSION);

    //---------------------------------------------------
    //Result Notifications
    //---------------------------------------------------
    function showResultText(result,textId){
        if(result == 1){
            console.log("Show Notification - OK");

            $("#actionStatusContainer").show();
            $("#actionStatusContainer").removeClass("hide-element");
            $("#actionStatusIcon").addClass("fa-check");
            $("#actionStatusIconContainer").addClass("update-success");
            $("#actionStatusText").text("Done!");
            $('#actionStatusContainer').delay(2000).fadeOut('slow');
        }else {
            console.log("Show Notification - ERROR");

            $("#actionStatusContainer").show();
            $("#actionStatusContainer").removeClass("hide-element");
            $("#actionStatusIcon").addClass("fa-exclamation");
            $("#actionStatusIconContainer").addClass("update-danger");
            $("#actionStatusText").text(result);
            $('#actionStatusContainer').delay(8000).fadeOut('slow');
        }
    }

    function init() {
        $("#input-id").fileinput();

        //---------------------------------------------------
        //Panel Buttons
        //---------------------------------------------------

        //Button Offset
        $("#btnOffset").click(function () {
            console.log("Button pressed: btnOffset");

            csInterface.evalScript('offsetHalf()', function(result) {
                showResultText(result,"OffsetText");
            });
        });

        //Button Equalize
        $("#btnEqualize").click(function () {
            console.log("Button pressed: btnEqualize");

            //var valueEqualize = $("#equalizeValue").val();
            var valueEqualize=100;
            csInterface.evalScript('equalizeImage("'+valueEqualize+'")', function(result) {
                showResultText(result,"EqualizeText");
            });
        });

        //Button Create AO / Cavity Map
        $("#btnCreateMaps").click(function () {
            console.log("Button pressed: btnCreateMaps");

            csInterface.evalScript('createMaps("'+extensionRoot+'")', function(result) {
                showResultText(result,"CreateMapsText");
            });
        });

        //---------------------------------------------------
        //Flyout Menu
        //---------------------------------------------------

        // Ugly workaround to keep track of "checked" and "enabled" statuses
        var checkableMenuItem_isChecked = true;
        var targetMenuItem_isEnabled = true;

        // Flyout menu XML string
        var flyoutXML = '<Menu> \
        <MenuItem Id="enabledMenuItem" Label="Enabled Menu Item" Enabled="true" Checked="false"/> \
        <MenuItem Id="disabledMenuItem" Label="Disabled Menu Item" Enabled="false" Checked="false"/> \
        \
        <MenuItem Label="---" /> \
        \
        <MenuItem Id="checkableMenuItem" Label="Checkable Menu Item" Enabled="true" Checked="true"/> \
        \
        <MenuItem Label="---" /> \
        \
        <MenuItem Id="actionMenuItem" Label="Click me to enable/disable the Target Menu!" Enabled="true" Checked="false"/> \
        <MenuItem Id="targetMenuItem" Label="Target Menu Item" Enabled="true" Checked="false"/> \
        \
        <MenuItem Label="---" /> \
        \
        <MenuItem Id="disabledMenuItem" Label="Texturing Tools v1.0" Enabled="false" Checked="false"/> \
        </Menu>';

        // Uses the XML string to build the menu
        csInterface.setPanelFlyoutMenu(flyoutXML);
        console.log("FlyoutMenu created");

        // Flyout Menu Click Callback
        function flyoutMenuClickedHandler (event) {
            // the event's "data" attribute is an object, which contains "menuId" and "menuName"
            console.dir(event);
            switch (event.data.menuId) {
                case "checkableMenuItem":
                checkableMenuItem_isChecked = !checkableMenuItem_isChecked;
                csInterface.updatePanelMenuItem("Checkable Menu Item", true, checkableMenuItem_isChecked);
                break;
                case "actionMenuItem":
                targetMenuItem_isEnabled = !targetMenuItem_isEnabled;
                csInterface.updatePanelMenuItem("Target Menu Item", targetMenuItem_isEnabled, false);
                break;
                default:
                console.log(event.data.menuName + " clicked!");
            }
            csInterface.evalScript("alert('Clicked!\\n \"" + event.data.menuName + "\"');");
        }

        // Listen for the Flyout menu clicks
        csInterface.addEventListener("com.adobe.csxs.events.flyoutMenuClicked", flyoutMenuClickedHandler);
    }
    init();
}());
